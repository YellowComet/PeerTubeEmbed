## 1.0.5

### Release notes

This release's main feature is running the URL generated for the iframe through HtmlArmor::getHtml() (basically a wrapper for htmlspecialchars()) before being put on the iframe.

I've not managed to get XSS yet, though I've been able to mangle the iframe tag by taking advantage of this. Regardless, it is a matter of time before someone finds a way to do it. You're encouraged to upgrade.

### Changelist

* Correct PHP sytax errors in README (https://codeberg.org/YellowComet/PeerTubeEmbed/pulls/14)

* Change PeerTubeEmbedHostFilterMode to a proper default (https://codeberg.org/YellowComet/PeerTubeEmbed/pulls/16)

* sanitize user input (https://codeberg.org/YellowComet/PeerTubeEmbed/pulls/17)

* Use the right class name for HtmlArmor (https://codeberg.org/YellowComet/PeerTubeEmbed/pulls/19, https://codeberg.org/YellowComet/PeerTubeEmbed/commit/9568a96f7227a8a16b8536462b4dc3c08e52557c)

## 1.0.4

### Release notes

This release adds the missing </iframe> closing tag. Without it, all content after the embed was included inside the embed, making the rest of the article appear missing.

This was caused by a lack of more thorough testing, I will be more careful in the future.

Other than that, there have been changes to the CHANGELOG.md file to clarify that contributions to this repo are under the GPL-2.0-or-later. The license notice at README.md has also been changed.

The URL at extension.json (the one shown at Special:Version) has also been changed to this extension's page at mediawiki.org.


(Correction February 12th, 2023: The changes clarifying the license of PRs were done to CONTRIBUTING.md, not CHANGELOG.md)

### Changelist

* Add closing </iframe> tag. (https://codeberg.org/YellowComet/PeerTubeEmbed/pulls/12)

* Replace license section with the GPL 2.0's recommended notice. (https://codeberg.org/YellowComet/PeerTubeEmbed/pulls/7)

* Clarify license at CONTRIBUTING.md. (https://codeberg.org/YellowComet/PeerTubeEmbed/pulls/8, https://codeberg.org/YellowComet/PeerTubeEmbed/pulls/9, https://codeberg.org/YellowComet/PeerTubeEmbed/commit/f2d54e3b8e0fdafc518e0491efdb3d949d25a2fb)

## 1.0.3

### Release notes

This release fixes fullscreen functionality, and sets up i18n.

### Changelist

* Fix fullscreen functionality (https://codeberg.org/YellowComet/PeerTubeEmbed/pulls/5).

* Setup i18n (https://codeberg.org/YellowComet/PeerTubeEmbed/pulls/4).

## 1.0.2

### Release notes

This release's most important change is the change from printf to sprintf, which avoids including the lenght of the string in the output.

On the documentation side of things, README.md has also seen some changes. Notably, it recommends downloading the release from the releases tab instead of cloning with Git. CHANGELOG.md has seen some minor changes related to Markdown formatting.

### Changelist

* Use sprintf instead of printf to avoid including the length of the formatted string in the output.

* Update CHANGELOG.md's syntax.

* Update README.md. Remove mention of cloning with Git, and change "article" in the usage section to "video".

## 1.0.1

Check if $parsedURL["path"] is set.
