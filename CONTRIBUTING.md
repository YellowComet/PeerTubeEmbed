Pull requests are always welcome.

# Copyright

By submitting a pull request to [this repository](https://codeberg.org/YellowComet/PeerTubeEmbed), you license your contributions under the GNU GPL 2.0, and aditionally grant the option to use any later version of the license, see the license notice at README.md.

Aditionally, you should add your name to [extension.json](https://codeberg.org/YellowComet/PeerTubeEmbed/src/branch/main/extension.json) in the author section (add a coma and put your name there), and you should add your name and email (or other suitable way to contact you) in the Copyright section of code files.

For example, if the Copyright section in [src/PeerTube.php](https://codeberg.org/YellowComet/PeerTubeEmbed/src/branch/main/src/PeerTube.php) currently contains:

```
Copyright 2023 Alex <alex@blueselene.com>
```

You should change it to

```
Copyright 2023 Alex <alex@blueselene.com>
Copyright 2023 Bob <bob@example.com>
```
