# PeerTubeEmbed

An extension for embedding PeerTube videos in MediaWiki articles.

# Installation

Download the latest release from [the Releases tab](https://codeberg.org/YellowComet/PeerTubeEmbed/releases), then make a directory called PeerTubeEmbed in your extensions folder and uncompress it there.

Then add `wfLoadExtension('PeerTubeEmbed');` to your LocalSettings.php file.

# Usage

Simply put `<peertube>URL</peertube>` wherever you want the video to be embedded.

For example: `<peertube>https://tilvids.com/w/vsMQtvP2Nc6dkKVue7dFt2</peertube>`

# Configuration

This extension has 3 configuration variables. You can add these to your LocalSettings.php file.

- `$wgPeerTubeEmbedDisableP2P`: If set to `true`, [disable P2P playback](https://joinpeertube.org/faq#what-are-the-main-advantages-of-peertube) in embeds. Example: `$wgPeerTubeEmbedDisableP2P = true;`.
- `$wgPeerTubeEmbedHostFilterMode`: Accepts one of two values, `allow` (`$wgPeerTubeEmbedHostFilterMode = "allow";`), which only creates the embed if the host is in the host filter, and `deny` (`$wgPeerTubeEmbedHostFilterMode = "deny";`), which refuses to create the embed if the host is in the host filter.
- `$wgPeerTubeEmbedHostFilterList`: An array holding a list of domains. How this array is interpreted depends on what `PeerTubeEmbedHostFilterMode` is set to. To add entries add `$wgPeerTubeEmbedHostFilterList[] = "DOMAINNAME.TLD";` on your LocalSettings.php. For example: `$wgPeerTubeEmbedHostFilterList[] = "tilvids.com";`.

See also [$wgCSPHeader](https://www.mediawiki.org/wiki/Manual:$wgCSPHeader).

# License

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
