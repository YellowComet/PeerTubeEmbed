<?php
// SPDX-License-Identifier: GPL-2.0-or-later
/*
Copyright 2023 Alex <alex@blueselene.com>

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/

use MediaWiki\MediaWikiServices;

class PeerTubeEmbed {
	public static function onParserFirstCallInit(&$parser) {
		$parser->setHook('peertube', [__CLASS__, 'embed']);
	}
	public static function embed($text, $params, $parser, $frame) {
		$config = MediaWikiServices::getInstance()->getConfigFactory()->makeConfig('PeerTubeEmbed');
		$disablep2p = $config->get('PeerTubeEmbedDisableP2P');
		$hostfiltermode = $config->get('PeerTubeEmbedHostFilterMode');
		$hostfilterlist = $config->get('PeerTubeEmbedHostFilterList');
		$parsedURL = parse_url($text);
		if (!$parsedURL) {
			//error when parsing
			return '';
		}
		//Look for required components
		if (!isset($parsedURL["path"])) {
			return '';
		}
		if (!isset($parsedURL["host"]) || !str_contains($parsedURL["path"], '/w/')) {
			return '';
		}
		//filter
		switch ($hostfiltermode) {
			case 'allow':
				if (!in_array($parsedURL["host"], $hostfilterlist)) {
					return '';
				}
				break;
			case 'deny':
				if (in_array($parsedURL["host"], $hostfilterlist)) {
					return '';
				}
				break;
		}
		//Extract the ID
		$videoid = substr($parsedURL["path"], 3); //remove /w/
		$embedURL = htmlspecialchars('https://' . $parsedURL["host"] . '/videos/embed/' . $videoid);
		if ($disablep2p) {
			return sprintf('<iframe class="PeerTubeEmbed" src="%s?warningTitle=0&amp;p2p=0" allow="fullscreen" sandbox="allow-same-origin allow-scripts allow-popups" width="560" height="315" frameborder="0"></iframe>', $embedURL);
		} else {
			return sprintf('<iframe class="PeerTubeEmbed" src="%s" allow="fullscreen" sandbox="allow-same-origin allow-scripts allow-popups" width="560" height="315" frameborder="0"></iframe>', $embedURL);
		}
	}
}
?>
